

## Direct Rendering

Direct Rendering lets an OpenGL client write to the 3D hardware without passing commands through the X Server. This is much faster than [[IndirectRendering|IndirectRendering]] but it can be very complicated to achieve, especially on multi-user systems like Unix. 



---

 [[CategoryGlossary|CategoryGlossary]] 
