

# Rendition

The Rendition Verite was an early competitor of the 3dfx Voodoo line.  The design was quite ahead of its time, being based on a general-purpose RISC core with additional instructions for 3d primitives.  Three models saw production: 

* Verite V1000 
* Verite V2100 
* Verite V2200 
The major difference between the V1000 and later chips is that the V1000 lacks a 'tri' instruction for hardware triangle setup.  The V2x00s appear to mostly differ by clock speed. 

Rendition was bought up by Micron. 


## Status

There is no DRI or DRM support for the Rendition chips.  Adding this support would involve figuring out how to load an acceptable microcode onto the chip.  This seems to be quite problematic, the 2D driver in Xorg doesn't use the accelerated 2D microcode at all. 

Since the hardware is very programmable, it would be a fascinating - if perhaps not very practical - experiment to implement vertex and fragment shaders in the Verite microcode. 



---

 [[CategoryHardwareChipset|CategoryHardwareChipset]] 
