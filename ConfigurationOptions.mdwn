

# Common Configuration Options

Below you find detailed descriptions of common configuration options. Along with the documentation of each option you find the name and the numeric option values to be used if you edit the configuration file in a text editor. 


## Performance Options


### TCL mode (Transformation, Clipping, Lighting)

* Name: tcl_mode 
* Drivers: r200, radeon 
This option defines whether a hardware TCL-unit is used and how the implementation uses it. Four settings are supported: 

* 0 = Software  
 1 = TCL stage in MESA pipeline  
 2 = Bypass MESA's pipeline  
 3 = Bypass MESA's pipeline with state-based code generation 
The default setting is 3 which results in best performance. If you experience rendering errors try a lower setting and report a bug, stating which setting results in what kind of rendering problems. 

**Note:** One well known problem is "depth fighting" observed in e.g. Tux Racer. We believe that the application is broken in this case. Set tcl_mode to 0 (Software) as a workaround. 

**Note 2:** As usual before reporting a bug please make sure that it is not a known problem. 


### Frame Throttling

* Name: fthrottle_mode 
* Drivers: r200, radeon 
This option defines what happens, if the application submits rendering commands faster than the graphics engine can execute them. Without limitations the graphics engine could be several seconds behind the application's command stream. In an interactive application this is not acceptable. Imagine a simulation in which you turn the steering wheel and it takes one second before your car reacts. Therefore the application is forced to wait if it gets more than one frame ahead of the graphics engine. This waiting can be done in three different ways: 

* 0 = Busy waiting  
 1 = Usleeps  
 2 = Software IRQs 
The default setting (Software IRQs) should be fine for most users. Usleeps can reduce the frame rate. Busy waiting doesn't reduce the frame rate but wastes CPU cycles in busy waiting loops (as the name indicates). 


### Synchronization with vertical refresh (swap intervals)

* Name: vblank_mode 
* Drivers: mga, r128, r200, radeon 
Synchronization with the vertical refresh can avoid visual "tearing" with fast motion. At the same time it limits the frame rate to (a fraction of) the vertical refresh rate. Applications can set a "swap interval" which means that buffer swaps occur no earlier than n vertical blanks after the previous swap. With this option you can disable swap intervals, choose a default swap interval of 0 or 1 or you can force the application to always wait for a vertical blank on every buffer swap: 

* 0 = Never, FPS rulez!  
 1 = Application preference, default interval 0  
 2 = Application preference, default interval 1  
 3 = Application preference, always synchronize with refresh 

## Image Quality Options


### Texture color depth

* Name: texture_depth 
* Drivers: mga, r128, r200, radeon 
This option influences the choice of the internal texture format based on the application-supplied texture data and the application's preference for the interal texture format. 

* 0 = Prefer frame buffer color depth  
 1 = Prefer 32 bits  
 2 = Prefer 16 bits  
 3 = Force 16 bits 
The first three settings affect the choice only if the application doesn't specify an internal texture color depth. The default setting 0 will prefer the same color depth as the frame buffer. The last setting forces 16 bit internal color depth even if the application requests a higher color depth. 

There is a trade-off between image quality and performance here. A higher color depth means higher demands for memory size and bandwidth. Low texture color depth is most noticable if your application has textures with smooth gradients on them. 


### Default color reduction method

* Name: color_reduction 
* Drivers: mga, r128, r200, radeon 
On a 16 bit frame buffer color reduction can take place in different ways. Colors can be rounded or truncated which means that if there is no perfect match for the desired color you will see a color that comes more or less close to it. This leads to visible edges in smooth gradients. The alternative color dithering can lead to large improvements of the visual quality. However, it may also introduce noise or a visible dithering pattern. The best choice may depend on the application. 

* 0 = Round or truncate  
 1 = Dither 

### Round or truncate colors

* Name: round_mode 
* Drivers: r128, r200, radeon 
If colors are rounded then this option decides how the displayed color is chosen if there is no perfect match for the desired color. 

* 0 = Truncate  
 1 = Round 
Truncate will round each color component to the closest value that is still lower than the desired value. Round will choose the closest available value. 


### Color dithering

* Name: dither_mode 
* Drivers: r128, r200, radeon 
If colors are dithered then this option chooses the dithering algorithm. 

* 0 = Horizontal error diffusion  
 1 = Horizontal error diffusion, reset error at line start  
 2 = Ordered 2D dithering 
The default setting 0 tries to compensate the rounding error of one pixel by biasing the rounding of the next pixel in the row. This may lead to a slightly noisy image and may introduce visible vertical patterns. Setting 1 is a variation that resets the error at the beginning of each new row. However, this setting looks broken on my Radeon 7500. Setting 2 uses a static pattern of rounding biases. This avoids the noise observed with setting 0 but the static pattern can be annoying espcially with low resolutions. 
