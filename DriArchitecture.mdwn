

# DRI Architecture

   * [[DriDriver|DriDriver]] 
         * XFree86 extensions: 
               * GLXExtension 
               * GLcoreExtension 
               * [[DriExtension|DriExtension]] 
         * libGL 
               * libGLDriver 
                     * [[MesaDriver|MesaDriver]] 
         * DRM 
               * DRMTemplates 