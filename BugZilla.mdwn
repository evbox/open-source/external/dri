
BugZilla is a commonly used bug report management system. 

DRI and Mesa bugs should be submitted at [[freedesktop.org's Bugzilla|http://bugs.freedesktop.org/]].  Bug reports submitted in this fashion will be automatically forwarded to the dri-devel mailing list.  Please submit all new bugs here. 

There are also older bug databases for DRI at [[xfree86.org|http://bugs.xfree86.org/]] and [[sourceforge.net|http://sourceforge.net/tracker/?atid=100387&group_id=387&func=browse]], but these are deprecated, and should not be used for submitting new bugs. 
