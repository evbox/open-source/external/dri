

# Chipset Name

* SiS 6326 
* SiS 530 

## Status

A driver for the SiS 6326 series is currently in development by [[EricAnholt|EricAnholt]]. [[AlanCox|AlanCox]] has ported it to run with XFree 4.3.0 and the sis-4.4.0 DRI module. Basic 3D stuff works with some depth and triangle setup problems, also no textures yet. 

Notable differences from the [[SiS300|SiS300]] series are that these cards have a single texture unit and a 16-bit Z buffer. A UtahGLX driver for these cards is available. 


## Specifications

   * [[SiS530|SiS530]] datasheet (using for [[SiS53fb|SiS53fb]] driver) 
   * [[http://softpixel.com/~cwright/papers/tech/0530DS10.PDF|http://softpixel.com/~cwright/papers/tech/0530DS10.PDF]] 6326 SiS datasheet (chipset) exe (self extracting) 
   * [[http://softpixel.com/~cwright/papers/tech/6326ds10.exe|http://softpixel.com/~cwright/papers/tech/6326ds10.exe]] 

## Resources

   * [[SiS homepage|http://www.sis.com/]] 
   * [[Thomas Winischhofer's SiS graphics chipsets and Linux/XFree86 page|http://www.winischhofer.net/linuxsisvga.shtml]] 
   * [[Current code drop from Alan Cox|http://www.linux.org.uk/~alan/sis6326.tar.gz]] 


---

 [[CategoryHardwareChipset|CategoryHardwareChipset]] 
