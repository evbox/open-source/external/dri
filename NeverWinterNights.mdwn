

# Neverwinter Nights (NWN)


## Well known 'error' message

**Problem**: 

   * I was still having a problem running Neverwinter Nights: when I ran the program I would simply get the useless 'Error' message, which should be well known to those in the NWN Linux community.  Why? 
**Solution** 

   * NWN did not work because it requests (since the beta4 client or so) a  32bit buffer size (though for no good reason, since it doesn't request  an alpha size, so 24bit would be enough, which would avoid all the  trouble...). However, up until very recently (last week in the cvs) the  X Server wrongly reported all true-color visuals with the root-depth  (which is only 24bit). This change is somewhere in the server side glx  code, and ends up afaik in libglx.a (btw if the snapshots don't contain  libglx.a then that will probably continue to be a problem - never tried  the snapshots myself, always using CVS). (You can easily check if you  have the fixed version with glxinfo - the "bf sz" is the value in  question.) The reason it works with older XFree86 versions is in old releases the client side code (I believe this all ends up in libGL.so?) (also errornously) simply ignores minimal buffer sizes asked by an application - thus the app could ask for a minimum buffer size of 32bit and just get back a 24bit buffer instead if no buffers with 32bit were available.  This was fixed some month ago and the client now gets back the error it should receive, which broke NWN. So, if you happen to have a libGL version with the glx visual matching code changes but not an X server which reports the buffer size correctly you have a problem. If this is still a problem today with the snapshots, then probably libglx.a (at least I think that's the correct file) is not included in the snapshots. -- [[RolandScheidegger|RolandScheidegger]] on dri-users. 

## Bad lightening

(maybe this is not the correct place for such a hint, but as there already was a NeverWinterNights section...) 

**Problem:** 

Some part of the scene is not lightened correctly (it is flickering).  

**Solution:** 

Seems that this is a problem with TCL. Just set R200_NO_TCL=1 (or use the [[ConfigurationOptions|ConfigurationOptions]] to disable TCL in newer version)  then it should work fine (but with no TCL of course). This problem is fixed in current CVS and snapshots, so disabling TCL should no longer be necessary. 

See this  [[NeverWinter Nights for linux FAQ|http://pgshopping.com/mdkxp/en/artcls/nwnfaq.php#path]] 

---

 [[CategoryTroubleshooting|CategoryTroubleshooting]] 
