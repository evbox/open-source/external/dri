# DRI Configuration Applet

DRIconf is a configuration applet for the Direct Rendering Infrastructure. It allows customizing performance and visual quality settings of OpenGL drivers on a per-driver, per-screen and/or per-application level. 

The settings are stored in system wide and per-user XML configuration files, which are parsed by the OpenGL drivers on startup. For more details see [[ConfigurationInfrastructure|ConfigurationInfrastructure]]. 

DRIConf is written in Python with the python-gtk toolkit bindings. 


## Download

* [[driconf-0.9.1.tar.gz|http://people.freedesktop.org/~fxkuehl/driconf/driconf-0.9.1.tar.gz]] (source) 
* [[http://people.eecs.ku.edu/~dhageman/driconf/|http://people.eecs.ku.edu/~dhageman/driconf/]] (rpms) 
* [[http://www.sisyphus.ru/srpm/driconf|http://www.sisyphus.ru/srpm/driconf]] (ALTLinux rpms) 

### Debian & Ubuntu Packages

Debian packages are available in Debian "etch" and unstable. Ubuntu has driconf in the "universe" repository. 


### Older Releases

* [[driconf-0.1.2.tar.gz|http://people.freedesktop.org/~fxkuehl/driconf/driconf-0.1.2.tar.gz]] (gtk-1.2, discontinued) 
* [[More old releases|http://people.freedesktop.org/~fxkuehl/driconf]] 

### SourceForge

DRIconf is now hosted on [[SourceForge|http://sourceforge.net/projects/driconf]]. The source code and the entire history (since version 0.0.2) is available in a Subversion repository and future releases will be available there as well. 


## Screenshot

[[!img driconf.png]]
 _Version 0.9.0_ 


## Installation Instructions

Pre-packaged versions of DRIConf are available for different Linux Distributions. Alternatively you can download the source tarball above and follow these instructions for installation. Before installing make sure that a Python version (>= 2.3) and the matching packages xml.parsers.expat and python-gtk2 version 2.4 or newer are installed. The installation uses Python's distutils package. 

Extract the archive and change into the source directory: 

    tar -xzf driconfig-x.y.z.tar.gz
    cd driconf-x.y.z

By default driconf will be installed into various subdirectories under /usr/local. You can change this behaviour in setup.cfg. In that case you may also have to adjust the driconf startup script accordingly. 

To start the installation run the following command as root: 

    python setup.py install

If everything goes well you should see something like this: 

    running install
    running build
    running build_py
    creating build
    creating build/lib
    copying dri.py -> build/lib
    copying driconf.py -> build/lib
    copying driconf_commonui.py -> build/lib
    copying driconf_complexui.py -> build/lib
    copying driconf_simpleui.py -> build/lib
    running build_scripts
    creating build/scripts-2.3
    copying and adjusting driconf -> build/scripts-2.3
    changing mode of build/scripts-2.3/driconf from 644 to 755
    running install_lib
    creating /usr/local/lib/driconf
    copying build/lib/driconf.py -> /usr/local/lib/driconf
    copying build/lib/driconf_commonui.py -> /usr/local/lib/driconf
    copying build/lib/dri.py -> /usr/local/lib/driconf
    copying build/lib/driconf_simpleui.py -> /usr/local/lib/driconf
    copying build/lib/driconf_complexui.py -> /usr/local/lib/driconf
    byte-compiling /usr/local/lib/driconf/driconf.py to driconf.pyc
    byte-compiling /usr/local/lib/driconf/driconf_commonui.py to driconf_commonui.pyc
    byte-compiling /usr/local/lib/driconf/dri.py to dri.pyc
    byte-compiling /usr/local/lib/driconf/driconf_simpleui.py to driconf_simpleui.pyc
    byte-compiling /usr/local/lib/driconf/driconf_complexui.py to driconf_complexui.pyc
    running install_scripts
    copying build/scripts-2.3/driconf -> /usr/local/bin
    changing mode of /usr/local/bin/driconf to 755
    running install_data
    creating /usr/local/share/driconf
    copying card.png -> /usr/local/share/driconf
    copying screen.png -> /usr/local/share/driconf
    copying screencard.png -> /usr/local/share/driconf
    copying drilogo.jpg -> /usr/local/share/driconf
    creating /usr/local/share/locale/de
    creating /usr/local/share/locale/de/LC_MESSAGES
    copying de/LC_MESSAGES/driconf.mo -> /usr/local/share/locale/de/LC_MESSAGES
    creating /usr/local/share/locale/es
    creating /usr/local/share/locale/es/LC_MESSAGES
    copying es/LC_MESSAGES/driconf.mo -> /usr/local/share/locale/es/LC_MESSAGES
    creating /usr/local/share/locale/it
    creating /usr/local/share/locale/it/LC_MESSAGES
    copying it/LC_MESSAGES/driconf.mo -> /usr/local/share/locale/it/LC_MESSAGES
    creating /usr/local/share/locale/ru
    creating /usr/local/share/locale/ru/LC_MESSAGES
    copying ru/LC_MESSAGES/driconf.mo -> /usr/local/share/locale/ru/LC_MESSAGES

After successful installation you can run driconf from the shell or install it in a menu of your window manager or desktop environment. Version 0.9.1 includes a driconf.desktop file that adds DRIconf to your Settings menu if you copy it to /usr/share/applications/driconf.desktop. 


## History


### Gtk-2 Versions


#### driconf-0.9.1: Sun Sep 17 22:04:22 EDT 2006

* Updated Russian translation by Konstantin A. Lepikhov. 
* Added Dutch translation by Benno Schulenberg. 
* Fixed a mistake in an English string (Benno Schulenberg). 
* Added an icon and a .desktop file by Pascal de Bruijn. 

#### driconf-0.9.0: Thu Jan 26 22:39:20 EST 2006

* This version introduces a completely redesigned user interface. If the old user interface looked more like the GNOME configuration editor then the new one is more like a configuration applet. The old user interface is still available as "expert mode". 
* Now requires pygtk 2.4 or newer. 
* Translations need updates! 
Changes in the old user interface (expert mode): 

* New and Delete buttons in Unknown tabs renamed to Add and Remove. 
* Only allow adding and renaming of unknown options if there is no driver information available. 
* Automatically turn entries into appropriate widgets when invalid values are replaced with valid ones by the user. 
* Fixed: prevent editing/adding/removing of unknown options in read-only files. 

#### driconf-0.2.7: Thu Aug 11 18:47:39 EDT 2005

* Italian translation by Giampaolo Bozzali. 
* Russian translation by Konstantin A. Lepikhov. 
* Changed installation. Python modules are now installed into /usr/local/lib/driconf. 

#### driconf-0.2.6: Thu Apr 14 01:08:41 CEST 2005

* Spanish translation by David Rubio Miguélez. 
* .po-files included in the source release. 
* Look for translations in the current directory first. This makes it easier to test new translations when older ones are installed in the standard place. 
* Use gtk-2.6 AboutDialog if available. 
* Changed handling of long tab labels. This gets rid of visual artifacts on truncated tab labels+tooltips with some gtk themes. 
* Fixed: lots of deprecation warnings with pygtk 2.6 about gtk.TRUE and gtk.FALSE. Now using True and False, requires Python 2.3. 
* Fixed: small problems with the Makefile for maintaining translations. 

#### driconf-0.2.5: Sun Mar 27 15:32:59 CEST 2005

* I18N support, initial German translation. More translations are welcome. 
* Eye-candy: icons, bold configuration file names in the configuration tree, bold application frame label. 
* Slightly larger initial window size, but allow resizing to smaller sizes. 
* No more annoying popup-dialog about unknown options. The "Unknown" page was moved to the first place for attention and contains a help-button that displays a short help text. 
* Unknown options are now editable, both their names and values, new settings can be added to the list of unknown options. 
* Allow configuration of unkown drivers the same way as unknown options of known drivers. 
* Allow changing the screen and driver of existing device configurations. 
* Invalid settings are no longer highlighted when disabled. 
* Fixed: spin-buttons for integers had a decimal point. 
* Fixed: markup in the configuration tree was broken (only visible with invalid settings). 
* Fixed: assertion failure when deleting driver or application configurations. 
* Fixed: escape all text that is passed to Pango as markup. 
* Fixed: there was a possibility that a read-only configuration was selected on startup even though a writable one existed. 

#### driconf-0.2.4: Fri Mar 18 12:50:22 CET 2005

* Added an "About" dialog. 
* Added some instructive text to device and application name dialogs. 
* Made the executable attribute more similar to a regular option. 
* Truncate long tab labels and add a tooltip with the full description. 
* More concise device descriptions in the configuration tree. 
* Updated README. 
* Fixed: Problem when closing the DRIconf window through the window manager. 
* Fixed: Wrong button activation after saving a configuration file. 

#### driconf-0.2.3: Mon Mar 14 02:13:30 CET 2005

* Added a new widget for float and integer ranges: spin button + slider. (The slider is not shown on small integer ranges.) 
* Usability improvements: Automatically select new nodes. Adjust scrolling of the configuration tree when the selection is changed automatically. 
* Updated README. Added a little help for getting started. 
* Fixed: A bad bug when entry widgets get used. 
* Fixed: Some (not all) deprecation warnings with gtk+ 2.6. 
* Fixed: Saving when a file node was selected in the configuration tree. 

#### driconf-0.2.2: Mon Jan  5 03:51:35 CET 2004

* Fixed a problem with floating point options that specify a list of valid values, like the new def_max_anisotropy. 

#### driconf-0.2.1: Sat Nov 15 10:04:37 CET 2003

* Select the first writable application configuration on startup with the nice side effect that the window doesn't grow later. 
* White background behind the logo. 
* Replaced the deprecated (in gtk2) CTree and CList widgets with TreeViews. 
* Completely rewired checking for invalid user input in Entry widgets. 

#### driconf-0.2.0: Tue Oct 28 15:10:20 CET 2003

* Ported driconf to gtk-2.0. 
* Made the configuration tree scrollable. 
* Allow multiple selection in unknown section page. 
* Fixed: reloading didn't work when a config file was selected. 

### Gtk-1.2 Versions

The Gtk-1.2 series of DRIconf has been discontinued. 


#### driconf-0.1.2: Mon Jan  5 03:56:51 CET 2004

* Fixed a problem with floating point options that specify a list of valid values, like the new def_max_anisotropy. 

#### driconf-0.1.1: Sat Nov 15 10:08:13 CET 2003

* Select the first writable application configuration on startup with the nice side effect that the window doesn't grow later. 
* White background behind the logo. 
* Completely rewired checking for invalid user input in Entry widgets. 

#### driconf-0.1.0: Tue Oct 28 15:15:34 CET 2003

* Made the configuration tree scrollable. 
* Allow multiple selection in unknown section page. 
* Fixed: reloading didn't work when a config file was selected. 

### Versions before the fork of separate Gtk-2 and Gtk-1.2 versions


#### driconf-0.0.11: Thu Oct 23 00:20:06 CEST 2003

* Toggle buttons: "True", "False" -> "Yes", "No". 
* Pushing "New" while an application is selected creates a new application in the same device. 
* Fixed: Handle options without any description. 
* Fixed: Entry widget always marked the configuration as modified. 
* Fixed: Message dialogs opened before the main window was created crashed. 
* Fixed: Segfault reloading a config file while an application was selected. 

#### driconf-0.0.10: Sat Oct 11 23:37:19 CEST 2003

* Added a "reload" button to reload a configuration from disk. 
* Added spaces to the toolbar. 
* Fixed: rename applications caused an exception (Michel Dänzer). 
* Fixed: Select gtk version 1.2 on systems that support version selection (Michel Dänzer). 
* Fixed: use getlocale(LC_MESSAGES) to select the language of option descriptions (Michel Dänzer) 

#### driconf-0.0.9: Fri Oct  3 14:16:21 CEST 2003

* Save button is only sensitive on modified configs. 
* No more annoying message box after saving successfully. 
* Ask before losing changes on exit. 
* Load configuration files before opening the main window. 
* Show a message box if the driver's config info has errors. 
* Make all dialogs transient windows for the main window. 
* Fixed: workaround for a segfault in libgtk when deleting application entries. 

#### driconf-0.0.8: Sun Sep 28 12:31:48 CEST 2003

* Added a special section page for options that appear in a configuration file but are unknown to the driver (only shows up if necessary). 
* Improved error handling/reporting in case of invalid driver config info. 
* Improved handling of invalid option values. 
* Highlight applications with invalid options in the configuration tree. 
* Ask before saving a configuration with invalid entries. 

#### driconf-0.0.7: Mon Aug 25 23:43:34 CEST 2003

* If an application or device is selected the save button saves the containing configuration file. 
* Tries to write the default configuration for non-existing configuration files on startup showing a message dialog if successful. 
* Non-existent and non-creatable config files don't show up in the config tree. 
* Existing but non-writable config files are read-only, all the toolbar buttons and configuration widgets are unsensitive. 
* Fixed: Error message about failed xdriinfo generated an exception itself :-/ 

#### driconf-0.0.6: Thu Aug 21 03:40:37 CEST 2003

* Added a custom GtkOptionMenu-like widget for enums that wraps lines 
* Display options' XML elements as tooltip 
* Make widgets of disabled options unsensitive 
* Fixed: workaround for a strange problem with a modal dialog opened in the select_row callback of a ctree with selection mode SELECTION_BROWSE. It locked up the entire X session in a way that no more input was accepted! 
* Fixed: popen2.Popen3 didn't seem to work from inside gtk callbacks. 

#### driconf-0.0.5: Sat Aug 16 15:41:12 CEST 2003

* Adjust toolbar button sensitivity to selected config tree item 
* Reset individual options to their default value 
* Display translated description as option label, name and type as tooltip 
* Use popen2.Popen3 so that xdriinfo's stderr can be captured to produce more helpful error messages 
* Fixed: show an error message dialog if xdriinfo fails on program startup 

#### driconf-0.0.4: Mon Jul 28 23:20:16 CEST 2003

* Support for enum options 
* Fixed: top level element must be <driconf> not <dri> 

#### driconf-0.0.3: Mon Jul 21 00:37:56 CEST 2003

* Appropriate widgets for specific option types and valid ranges 
* Changed options get activated automatically 
* Added a DRI logo 
* Installation using Python's distutils 
* Handles invalid configuration files gracefully 

#### driconf-0.0.2: Sun Jul 13 23:35:00 CEST 2003

* For non-existent files create one device per screen each with one empty application config 
* Fixed: Select a default language (en) and encoding (iso-8859-1) if not defined by the locale (e.g. "C") 

#### driconf-0.0.1: Sun Jul 13 19:29:12 CEST 2003

* Added CHANGELOG 
* Added TODO 
* Fixed: Forgot to commit changed entries before saving 
* Fixed: Install to $PREFIX/lib/$PYTHON/site-packages. It seems .pyo files are ignored. Install .pyc files instead. 

#### driconf-0.0.0: Sun Jul 13 17:00:00 CEST 2003

First release 
