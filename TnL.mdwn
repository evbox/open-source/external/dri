

## Transform & Lighting (T&L)

This is a technique where the 3D card does even more of the 3D calculations. This eases the load on the host CPU which results in significant speed increases for certain applications (especially games). 



---

 [[CategoryGlossary|CategoryGlossary]] 
