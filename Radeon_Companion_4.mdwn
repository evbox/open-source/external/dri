

# The irregular Radeon-Development companion


## Issue for 2007-04-25


## Intro

This page is an accumulation of the events happening in Radeon development, mainly on IRC or on dri-devel. The concept has been shamelessly copied from [[nouveau|http://nouveau.freedesktop.org]] and will hopefully be kept up at a frequent rate. But that (as everything in open source depends on time). 


## Status

* Olliver [[McFadden|McFadden]] did some work on his [[tool|http://gitweb.freedesktop.org/?p=users/z3ro/revenge.git;a=summary]] for reverse engineering lately. He is focussing on comparing the logs of fglrx with the ones from r300. He already found some minor differences and brought up questions about the major differences about the driver internals. While fglrx mainly uses indirect buffers to put data into to the card, r300 uses so called type one packets to transfer the data. Dave Airlie (airlied) explained that there is no big difference (performancewise) in any of these approaches. It's just a different way. Olliver [[McFadden|McFadden]] pointed out that this at least creates a very different output between fglrx and r300. It was agreed that we cannot and won't change the driver to mimik fglrx for no real benefit. 

* Dave Airlie (airlied) started working on RS480. This chipset is R300 based but is some kind of "stripped down". The PCIGart has to be set up differently and it lacks some TnL hardware. He got the modesetting more or less working and got an unaccellerated DDX (2D driver) up and running. He started looking on how to get 3D running. Take a look at the picture to see how glxgears currently look on RS480. 

[[!img http://www.skynet.ie/~airlied/dri/gears_rs480.png]  

You should known what it ormally it should look like... The main issue seems to be that fact that RS480 does not have any vertex shaders which will require a lot of work to get RS480 to run with the r300 driver. Dave Airlie (airlied) did not go any further yet. 

* On the modesetting front we got some exciting news lately. Shortly after Jakob Bornecrantz (Wallbraker) and Jesse Barnes (jbarnes) created a [[drm branch|http://gitweb.freedesktop.org/?p=mesa/drm.git;a=shortlog;h=modesetting-101]] for modesetting, Dave Airlie (airlied) dropped in since he needed it for an X-less GLX setup (called MiniGLX). He got it to work on Intel APG with Intel Graphics. Others (like Kristian Høgsberg or Alan Hourihane) partly reviewed the code and fixed some minor issues. This branch requires the AGP backend to support TTM (the new memory manager). Currently using drm modesetting is only possible on intel hardware. Thomas Hellstrom has a patch for via AGP to add support for TTM. Some people are trying to get Dave Airlie (airlied) to do the R300 work, but they did not have success with it (yet?). 

* The logging of the #dri-devel channel on irc.freenode.net can now be reached at a [[new location|http://dri.freedesktop.org]]. Christoph Brill (egore) setup up the logs and thought a simple URL would be usefull. 

* Currently people are thinking about R300 "marketing". The nouveau driver gained a lot of interest lately. People with ATI cards don't want to see R300 development fall behind and so recently a discussion started about what can be done to get the driver development going faster, better and greater. Trevinho came around on IRC and talked to Jerome Glisse (glisse) about what could be done to increase popularity. A good suggestion by Jerome Glisse was to setup a blog that is aggregated on planet.freedesktop.org that outlines the news like we do here in this TiRDC. Dave Airlie (airlied) is already blogging the "great news" in driver development on [[his blog|http://airlied.livejournal.com/]], but others don't do it. There was no decision made on how to progress. We will discuss this in a later issue. 

* The status of GLSL was one of the topics lately. Intel got GLSL done and it seems to work pretty flawless. Arc dropped in on IRC and asked what would be necessary to get R300 to work with GLSL. Dave Airlie (airlied) pointed out that we don't have any specs on how to do that on R300. Olliver [[McFadden|McFadden]] also pointed out that GLSL code is converted to vertex- and fragment-programs so we might need to fix them first. He also pointed out that R300 hardware does not support loops, so it would be necessary to unwrap the loops first. Arc was pointed to the Intel sources which are a good source for free 3D development. 


## Help wanted and needed

R300 does not have any real "marketing". If you can think of anything that will get the driver further, feel free to discuss this on #dri-devel or the mailing lists. It would be also really helpful if people would link to this site and increase the publicity of TiRDC. Jerome Glisse will try to inform some bigger Linux related sites about it. If you got contact to someone who might write something about R300 or DRI development, please inform them about this little site. 
