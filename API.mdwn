

## Application Programming Interface (API)

Common name used to describe the interface a programmer uses to access a library. Common 3D API's include OpenGL, Direct3D, QuickDraw3D, Renderman, Glide, and there are dozens of other lesser known ones. The two most popular real-time 3D API's these days are OpenGL and Direct3D. Glide enjoyed a dominant but brief popularity on the PC platform in the late 1990s. 



---

 [[CategoryGlossary|CategoryGlossary]] 
