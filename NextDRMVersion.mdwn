
This is a page to put ideas about future changes to the DRM should a major version number bump occur.  I've put some things I've come across while doing FreeBSD DRM work. 

* We should attach a DRM to a specific card, and not use the getunique/setunique mess that's currently used to attach DRMs to cards.  This is mostly doable for the single-DRI-head case while retaining backwards compatibility. (in progress --[[EricAnholt|EricAnholt]]) 
* DRM_IOCTL_IRQ_BUSID gets an irq for a pci device based on a busid.  This was useful when it was a generic DRM module function, and could service several different devices.  It should probably be changed to something that only returns the irq number of the device that that drm_device_t is attached to.  It might not be necessary at all and could be part of DRM_IOCTL_CONTROL. (in progress --[[EricAnholt|EricAnholt]]) 
* Actually, why not have DRM_IOCTL_CONTROL's contents just be moved into device setup/teardown (first open/last close)?  Is there any situation where ioctls would be enabled at a significantly different time than setup/teardown of the dri in the server?  Well, since the irq handlers require the device private to be initialized, it can't be moved easily (without changing how devices get initialized). 
* We should convert the gamma driver to a similar DMA infrastructure as the other devices if possible, greatly simplifying much of the general DRM code.  FreeBSD has axed much of the gamma DMA infrastructure from its DRM, but some parts can't be removed (such as SIGIO handler management) because the X Server still expects it. 
Miscellaneous cleanups: 

* Card drivers should use family information based on the pci device id in the kernel rather than taking in information from userland.  Quirks for hardware may require pci device id checks anyway. 
* It should work. 
There is a proposed new interface design up at DRMRedesign. 
