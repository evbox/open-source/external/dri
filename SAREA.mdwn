

## SAREA

The SAREA is a block of shared memory that is used to store internal device specific state but there's no simple answer to what, since it is used differently by each driver. It is shared among the X server and all the 3D clients. One piece that is common to all drivers is the upper layer of the lock. Reading the lock value out of the SAREA allows for fast locking. If there's contention the clients make an ioctl call to resolve the lock. 

Beyond that it depends on the driver. The TDFX driver only uses a few flags in the SAREA, one to indicate that the FIFO has been modified, one to indicate that the 3D state has been modified, and one to indicate texture state has been modified. That way the clients know how much state they need to update when a context switch occurs. 

Other drivers store more state in the SAREA. You can also store that state in the kernel driver. It's a design decision. So what values you put where will vary. 



---

 [[CategoryGlossary|CategoryGlossary]] 
