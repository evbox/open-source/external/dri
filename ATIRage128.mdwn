

# ATI Rage 128


## Status

The ATI Rage128 desktop and mobility chips are supported by the DRI.  [[EricAnholt|EricAnholt]] recently added pageflipping support. You can enable pageflipping by adding this to your config:

    Option "EnablePageFlip" "true"

It will give you a small boost in performance, but it causes problems for some people. 

There is no support for suspend/resume for r128.  It shouldn't be too hard to add support.  Take a look at the radeon suspend/resume code.  I think Xv also needs some fixes for suspend/resume. 

Todo: implement hardware-accelerated projective texturing 


## Specifications

The register documentation is available on Wikipedia 

[[http://en.wikipedia.org/wiki/Rage_128|http://en.wikipedia.org/wiki/Rage_128]] 


## Resources



---

 [[CategoryHardwareChipset|CategoryHardwareChipset]] 
