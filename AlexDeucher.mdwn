

## Alex Deucher

Email: agd5f at yahoo dot com 

Developer and maintainer of MergedFB support for ATIRadeon hardware and general Xorg developer.   

[[S3Savage|S3Savage]] DRI support - available in Xorg 6.9.0+ 

[[S3Savage|S3Savage]] EXA support - available in xorg cvs 

[[S3Savage|S3Savage]] [[Dualhead (Duoview) support|http://www.botchco.com/alex/new-savage/html/]] - available in Xorg 6.9.0+ 

ATIRage128 [[Dualhead Support|http://www.botchco.com/alex/r128]] - available in Xorg 6.9.0+ 

[[SiliconMotion|SiliconMotion]] Dualhead Support - in progress 

ATIMach64 Dualhead Support - no hardware 
